module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      build:{
        src: [
          'src/js/partials/state.js', 
          'src/js/partials/utils.js',
          'src/js/components/Swiper.js',
          'src/js/components/Navigation.js', 
          'src/js/components/Accordion.js', 
          'src/js/components/Dropdown.js',
          'src/js/components/Carousel.js',
          'src/js/components/SliderCarousel.js', 
          'src/js/components/Grid.js',
          'src/js/components/HeightMatcher.js',
          'src/js/jtui.js'
        ],
        dest: '../foursquare_contenthub/insights/static/js/<%= pkg.buildName %>.js'
      }
    },

    sass: {
      dist:{
        options: {                        
          style: 'expanded'
        },
        files: {                         
          '../foursquare_contenthub/insights/static/css/<%= pkg.buildName %>.css': 'src/scss/<%= pkg.srcName %>.scss'
        }
      }
    },

    watch: {
      js: {
        files: ['src/**/*.js'],
        tasks: ['concat']
      },
      sass: {
        files: ['src/**/*.scss'],
        tasks: ['sass']
      }
    }
  });

  // Load plugins:
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');

  // Register tasks
  grunt.registerTask('default', ['concat','sass']);
};