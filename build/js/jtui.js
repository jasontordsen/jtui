
/* === STATE =============================== */
/* ========================================= */

var JTUI = {

	didresize : true,

	didscroll : true,

	resizeto : null,

	mousedidmove : false,

	browser : {},

	currentframe : 0,

	laststeptime : 0,

	dropdowns : [],

	accordions : [],

	carousels : [],

	slider_carousels : [],

	height_matchers : []
}

/*======== EVENT HANDLERS ==========
===================================*/

JTUI.handleScroll = function( _e ) {

	JTUI.didscroll = true;
}

JTUI.onDidScroll = function() {

}

JTUI.handleResize = function( _e ) {

	clearTimeout( JTUI.resizeto );

	JTUI.resizeto = setTimeout( function() {

		JTUI.didresize = true;
	}, 1000);
}

JTUI.onDidResize = function() {
	console.log("onDidResize");

	$.each(JTUI.height_matchers, function(){
		this.onResize();
	});

	$.each(JTUI.carousels, function(){
		this.onResize();
	});

	$.each(JTUI.dropdowns, function(){
		this.onResize();
	});

	JTUI.onDidScroll();
}

/*======== ANIMATION LOOP ==========
  ===================================*/

JTUI.step = function(time) {
	if ( Math.round(JTUI.currentframe) % 20 === 0 && JTUI.didresize ) {

	    JTUI.onDidResize();

	    JTUI.didscroll = true;

	    JTUI.didresize = false;
	}

	if ( Math.round(JTUI.currentframe) % 20 === 0 && JTUI.didscroll ) { 

	    JTUI.onDidScroll();

	    JTUI.didscroll = false;
	}

	JTUI.laststeptime = time;

	JTUI.currentframe++;

	window.requestAnimationFrame( JTUI.step );
}

/* === UTILS ============================ */
/* ====================================== */

JTUI.getUserAgent = function() {

    JTUI.useragent = navigator.userAgent;

    //MOBILE
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        JTUI.useragent
      )
    )
      mobile = true;

    //GET BROWSER NAME
    if ( /Mozilla/i.test(JTUI.useragent) ) JTUI.browser.name = "Firefox";
    if ( /Safari/i.test(JTUI.useragent) ) JTUI.browser.name = "Safari";
    if ( /Chrome/i.test(JTUI.useragent) ) JTUI.browser.name = "Chrome";
    if ( /Android/i.test(JTUI.useragent) ) JTUI.browser.name = "Android";
    if ( /MSIE|Trident/i.test(JTUI.useragent) ) JTUI.browser.name = "IE";

    //GET BROWSER VERSION
    var start_i = 0,
    dot_i = 0,
    v_search_str = null;

    switch (JTUI.browser.name) {
      case "Firefox":
        JTUI.browser.prefix = { js: "Moz", css: "-moz-" };
        v_search_str = "Firefox/";
        break;
      case "Safari":
        JTUI.browser.prefix = { js: "webkit", css: "-webkit-" };
        v_search_str = "Version/";
        break;
      case "Chrome":
        JTUI.browser.prefix = { js: "webkit", css: "-webkit-" };
        v_search_str = "Chrome/";
        break;
      case "Android":
        JTUI.browser.prefix = { js: "webkit", css: "-webkit-" };
        v_search_str = "Android ";
        break;
      case "IE":
        JTUI.browser.prefix = { js: "ms", css: "-ms-" };
        if (/MSIE/i.test(JTUI.useragent)) v_search_str = "MSIE ";
        else JTUI.browser.version = "11.0";
        break;
    }

    if ( v_search_str ) {

      start_i = JTUI.useragent.indexOf(v_search_str);

      dot_i = JTUI.useragent.indexOf(".", start_i);

      JTUI.browser.version = JTUI.useragent.substring(
        start_i + v_search_str.length,
        dot_i + 2
      );
    }
  }


/* === SWIPER ============================== */
/* ========================================= */

JTUI.Swiper = function( _el, _handleSwipe ){

  this.start = null;

  this.end = null;

  this.onSwipe = _handleSwipe;

  $(_el).on( 'touchstart', this.onTouchStart.bind(this) );
  $(_el).on( 'touchmove', this.onTouchMove.bind(this) );
  $(_el).on( 'touchend', this.onTouchEnd.bind(this) );
}

JTUI.Swiper.prototype.onTouchStart = function(e){

    if( e.touches && e.touches[0] ){

        this.start = e.touches[0];

        this.start.time = e.timeStamp;
    }
}

JTUI.Swiper.prototype.onTouchMove = function( e ){

    if( e.touches && e.touches[0] ){

        this.end = e.touches[0];

        this.end.time = e.timeStamp;
    }
}

JTUI.Swiper.prototype.onTouchEnd = function( e ){

    if( this.start && this.end ){

        var direction = null, duration = this.end.time - this.start.time;

        if( duration > 100 && duration < 400 ){

            dist = { 
                x : this.end.clientX - this.start.clientX, 
                y : this.end.clientY - this.start.clientY 
            }

            if( Math.abs( dist.x ) > 100 && Math.abs( dist.y ) < 50 ){

                direction = dist.x < 0 ? "left" : "right";

            } else if( Math.abs( dist.y ) > 50 ){

                direction = dist.y > 0 ? "down" : "up";
            }

            if( direction ) this.onSwipe( direction );

            this.touch = null;
        }
    }
}

/* === DOT NAV =========================== */
/* ========================================= */

JTUI.DotNav = function( _length, _el, _onClick ){

	this.activeIndex = null;

	this.$el = $(_el);

	this.handleClick = _onClick;

	this.update( _length );
}

JTUI.DotNav.prototype.update = function( _length ){
	if( this.dots ){
		$.each( this.dots, function(i, dot){
			$(dot).off();
		});
	}

	this.$el.empty();

	this.dots = [];

	for( var i = 0; i<_length; i++ ){
		var _dot = $("<li>");

		_dot.click(function(e){
			this.handleClick( $(e.target).index() );
		}.bind( this ));

		this.$el.append(_dot);

		this.dots.push( _dot );
	}

	this.setActive( this.activeIndex || 0 );
}

JTUI.DotNav.prototype.setActive = function( _index ){
	if( this.activeIndex !== null )
		this.dots[ this.activeIndex ].removeClass("active");

	this.activeIndex = _index;

	if( !this.dots[ this.activeIndex ].hasClass("active") )
		this.dots[ this.activeIndex ].addClass("active");
}

/* === ARROW NAV =========================== */
/* ========================================= */

JTUI.ArrowNav = function( _el, _onClick ){

	this.$el = $(_el);

	this.$el.find(">li").click( function(e){

		_onClick( $(e.currentTarget).index() == 0 ? "left" : "right" );
	});
}

/* === ACCORDION =========================== */
/* ========================================= */

JTUI.Accordion = function( _el ){

  this.el = _el;

  this.$el = $(this.el);

  this.groups = this.$el.find(">.group");

  this.$el.find("button").click( this.handleClick.bind(this) );

  this.collapse = this.$el.data("collapse");

  if( this.collapse === false ) this.toGroup( 0 );
}

JTUI.Accordion.prototype.handleClick = function(e){
  
  var _index = $(e.currentTarget).parent(".group").index();

  this.toGroup( _index );
}

JTUI.Accordion.prototype.toGroup = function( _index ){

    if( this.collapse !== false || this.active !== _index  )
      this.deactActGroup();

    if( this.active !== _index  ){

      this.active = _index;

      this.actActGroup();
    }
}

JTUI.Accordion.prototype.deactActGroup = function(){

  if( this.active !== undefined && this.active !== null ){

    var _group = this.groups.eq( this.active ),

    _panel = _group.find(">.panel").eq(0);

    if ( _group.hasClass("active") )
      _group.removeClass("active");

    if( _panel ) _panel.css( "max-height", "0px" );

    this.dispatchResize();
  }
}

JTUI.Accordion.prototype.actActGroup = function(){

  if( this.active !== undefined && this.active !== null ){

    var _group = this.groups.eq( this.active ),

    _panel = _group.find(">.panel").eq(0);

    if ( !_group.hasClass("active") )
      _group.addClass("active");

    if( _panel ) _panel.css( "max-height", _panel.children().eq(0).outerHeight() + "px" );

    this.dispatchResize();
  }
}

JTUI.Accordion.prototype.dispatchResize = function(){

  if( this.timeout ) clearTimeout( this.timeout );

  this.timeout = setTimeout( JTUI.onDidResize, 300 );
}

/* === DROPDOWN ============================ */
/* ========================================= */

JTUI.Dropdown = function( _el ){

  this.el = _el;

  this.$el = $(this.el);

  this.$content_el = this.$el.find(">.dropdown-content").eq(0);

  this.$content_inner_el = this.$content_el.children().eq(0);

  this.$button_el = this.$el.find(".trigger");

  this.$button_el.click( this.handleClick.bind(this) );
}

JTUI.Dropdown.prototype.handleClick = function(e){

  if ( this.$el.hasClass("active") ) {

    this.$el.removeClass("active");

    this.$content_el.css("max-height", "0");

  } else {

    this.$el.addClass("active");

    var _ci_height = this.$content_inner_el.outerHeight();

    this.$content_el.css("max-height", _ci_height + "px");
  }
}

JTUI.Dropdown.prototype.onResize = function(){

  if ( this.$el.hasClass("active") ) {

    this.$content_el.css("max-height", this.$content_inner_el.outerHeight() + "px");
  }
}

/* === CAROUSEL =========================== */
/* ========================================= */

JTUI.Carousel = function( _el ){

  this.activeSlide = null;

  this.el = _el;

  this.$el = $(this.el);

  this.$inner_el = this.$el.find(">.jt-carousel-inner").eq(0);

  this.slides = this.$inner_el.find(">.jt-carousel-slide");

  this.dot_nav_el = this.$el.find("ul.jt-dot-nav")[0];

  this.arrow_nav_el = this.$el.find("ul.jt-arrow-nav")[0];

  this.dot_nav = new JTUI.DotNav( this.slides.length, this.dot_nav_el, this.toSlide.bind(this) );

  this.arrow_nav = new JTUI.ArrowNav( this.arrow_nav_el, this.onArrowClick.bind(this) );

  this.swiper = new JTUI.Swiper( this.el, this.onSwipe.bind(this) );

  this.$inner_el.css({
		"width": this.slides.length*100 + "%",
		"max-height" : this.$inner_el.outerHeight()
	});

  this.slides.css("width", 100/this.slides.length + "%");

  this.toSlide(0);
}

JTUI.Carousel.prototype.onResize = function(){
	
	if( this.activeSlide != null ){

		var slide = this.slides.eq( this.activeSlide );

		this.$inner_el.css({
			"max-height" : slide.outerHeight()
		});
  	}
}

JTUI.Carousel.prototype.onArrowClick = function( _direction ){

	this.onSwipe( _direction == "left" ? "right" : "left" );
}

JTUI.Carousel.prototype.onSwipe = function( _direction ){

	if(!_direction) return null;

	switch( _direction ){
		case "right":
			this.previousSlide();
		break;

		case "left":
			this.nextSlide();
		break;
	}
}

JTUI.Carousel.prototype.toSlide = function( _index ){

	if(this.activeSlide !== _index){

		this.deactActiveSlide();

		this.activeSlide = _index;

		this.actActiveSlide();
	}
}

JTUI.Carousel.prototype.nextSlide = function( _index ){

	this.deactActiveSlide();

	this.activeSlide++;

	if( this.activeSlide > this.slides.length-1 ) 
		this.activeSlide = 0;

  	this.actActiveSlide();
}

JTUI.Carousel.prototype.previousSlide = function( _index ){

  	this.deactActiveSlide();

	this.activeSlide--;

	if( this.activeSlide < 0 ) 
		this.activeSlide = this.slides.length-1;

  	this.actActiveSlide();
}

JTUI.Carousel.prototype.deactActiveSlide = function( ){

	if( this.activeSlide != null )
		this.slides.eq(this.activeSlide).removeClass("active");
}

JTUI.Carousel.prototype.actActiveSlide = function( ){

	if( this.activeSlide != null ){

		var slide = this.slides.eq( this.activeSlide );

	  	if( !slide.hasClass("active") )
  			slide.addClass("active")

  		this.dot_nav.setActive( this.activeSlide );

		var x = "translateX(" + (-100/this.slides.length*(this.activeSlide)) + "%)";
		  
  		this.$inner_el.css( { 
			"max-height" : slide.outerHeight(),
  			"transform":x, 
  			"-webkit-transform":x,
  			"-ms-transform":x,
  			"-moz-transform":x 
  		} );
  }
}




/* === SLIDER CAROUSEL =========================== */
/* ========================================= */

JTUI.SliderCarousel = function( _el ){

    this.activeSlide = null;

    this.columns = 3;

    this.slideBy = this.columns-1;

    this.timeout_delay = 100;

    this.el = _el;
  
    this.$el = $(this.el);
  
    this.$slides_el = this.$el.find(">.jt-carousel-content>.jt-carousel-slides-wrapper>.jt-carousel-slides").eq(0);
  
    this.slides = this.$slides_el.find(">.jt-carousel-slide");

    this.num_slides = this.slides.length;

    this.dot_nav_el = this.$el.find("ul.jt-dot-nav")[0];
  
    this.arrow_nav_el = this.$el.find("ul.jt-arrow-nav")[0];
  
    this.arrow_nav = new JTUI.ArrowNav( this.arrow_nav_el, this.onArrowClick.bind(this) );
  
    this.swiper = new JTUI.Swiper( this.el, this.onSwipe.bind(this) );
  
    this.build();
  }

  JTUI.SliderCarousel.prototype.build = function(){

    if( this.num_slides < this.columns ) this.columns = this.num_slides; 

    $.each( this.slides, function(){
        $( this ).attr( "data-slide-index", $(this).index() );
    });

    if( this.dot_nav ){
        
        this.dot_nav.update( this.num_slides / this.slideBy );
    } else {
        
        this.dot_nav = new JTUI.DotNav( 
            this.num_slides / this.slideBy, 
            this.dot_nav_el, 
            this.toSlide.bind(this)
        );
    }

    this.end_clones = this.slides.slice( 0, this.columns * 2 ).clone().addClass( "clone" );

    this.start_clones = this.slides.slice( -this.columns*2 ).clone().addClass("clone");

    this.$slides_el.prepend( this.start_clones ).append( this.end_clones );

    this.slides = this.$slides_el.find( ">.jt-carousel-slide" );

    this.$slides_el.css({
        "width": ( this.slides.length / this.columns ) * 100 + "%"
    });

    this.slides.css("width", 100/this.slides.length + "%");

    this.toFirstRealSlide();
  }
  
  JTUI.SliderCarousel.prototype.toFirstRealSlide = function(){
    
    if( !this.$slides_el.hasClass( "kill-transition") )
        this.$slides_el.addClass( "kill-transition");

    this.activeSlide = this.start_clones.length + 1 - this.columns;

    this.actActiveSlide();

    this.next_prev_timeout = setTimeout( this.next.bind(this), this.timeout_delay );
  }

  JTUI.SliderCarousel.prototype.onResize = function(){
      
  }
  
  JTUI.SliderCarousel.prototype.onArrowClick = function( _direction ){
  
      this.onSwipe( _direction == "left" ? "right" : "left" );
  }
  
  JTUI.SliderCarousel.prototype.onSwipe = function( _direction ){
  
      if(!_direction) return null;
    
      if(this.next_prev_timeout ) clearTimeout( this.next_prev_timeout );

      switch( _direction ){
          case "right":
            this.next_prev_timeout = setTimeout( this.previous.bind(this), this.timeout_delay );
          break;
  
          case "left":
            this.next_prev_timeout = setTimeout( this.next.bind(this), this.timeout_delay );
          break;
      }
  }
  
  JTUI.SliderCarousel.prototype.toSlide = function( _index ){
  
    if(this.activeSlide !== _index){
  
        this.activeSlide = _index;
  
        this.actActiveSlide();
    }
  }
  
  JTUI.SliderCarousel.prototype.next = function(){
    
    var first_real_index = this.start_clones.length,
    
    last_real_index = first_real_index + this.num_slides - 1;
    
    if( this.activeSlide > last_real_index ){

        var real_slide_index =  first_real_index + (this.activeSlide - (last_real_index + 1));

        if( !this.$slides_el.hasClass( "kill-transition") )
            this.$slides_el.addClass( "kill-transition");

        this.activeSlide = real_slide_index;

        this.actActiveSlide();

        if(this.next_prev_timeout ) clearTimeout( this.next_prev_timeout );

        this.next_prev_timeout = setTimeout( this.next.bind(this), this.timeout_delay );
    } else {

        if( this.$slides_el.hasClass( "kill-transition") )
            this.$slides_el.removeClass( "kill-transition");

        this.activeSlide += this.slideBy;

        this.actActiveSlide();
    }
  }
  
  JTUI.SliderCarousel.prototype.previous = function(){

    var first_real_index = this.start_clones.length;

    if( this.activeSlide <= first_real_index - this.columns ){

        var real_slide_index =  this.start_clones.length + this.activeSlide;

        if( !this.$slides_el.hasClass( "kill-transition") )
            this.$slides_el.addClass( "kill-transition");

        this.activeSlide = real_slide_index;

        this.actActiveSlide();

        if(this.next_prev_timeout ) clearTimeout( this.next_prev_timeout );

        this.next_prev_timeout = setTimeout( this.previous.bind(this), this.timeout_delay );
    } else {

        if( this.$slides_el.hasClass( "kill-transition") )
            this.$slides_el.removeClass( "kill-transition");

        this.activeSlide -= this.slideBy;

        this.actActiveSlide();
    }
  }
  
  JTUI.SliderCarousel.prototype.actActiveSlide = function( ){
  
    if( this.activeSlide != null ){

        var slide_x = "translateX(" + this.activeSlide * ( -100 / this.slides.length ) + "%)";
            
        this.$slides_el.css( { 
            "transform":slide_x, 
            "-webkit-transform":slide_x,
            "-ms-transform":slide_x,
            "-moz-transform":slide_x 
        } );
    }
  }

/* === GRID ================================ */
/* ========================================= */

JTUI.Grid = function( _el ){

 this.el = _el;

 this.$el = $(_el);

 this.cols = this.$el.find(">.jt-col");

 this.update();
}

JTUI.Grid.prototype.update = function(){
	
}
/* === HEIGHT WATCHER ============================== */
/* ========================================= */

JTUI.HeightMatcher = function( _el ){

    this.el = _el;

    this.$el = $(this.el);

    this.children = this.$el.find(">.jt-match-height");

    this.onResize();
  }
  
  JTUI.HeightMatcher.prototype.onResize = function(e){
    var _row_heights = [], _els = [];

	//Set natural height, then set to 150 to get desired row number.
    this.children.each(function(i, v) {
      var _el = $(v);

      _el.height("auto");

      _els.push({ _el: _el, natural_height: _el.outerHeight() });

      _el.height(150);
    });

    // Get row numbers
    $.each(_els, function(i, v) {
      v.row = Math.abs(Math.ceil(v._el.offset().top / 10) * 10);

      if (!_row_heights[v.row] || _row_heights[v.row] < v.natural_height)
        _row_heights[v.row] = v.natural_height;
    });

    // Set the height to row height
    $.each(_els, function(i, v) {
      v._el.height(_row_heights[v.row]);
    });
  }


/* === INIT =============================== */
/* ========================================= */

$(function() {

  $(".jt-dropdown").each(function(){

    JTUI.dropdowns.push( new JTUI.Dropdown( this ) );
  });

  $(".jt-accordion").each(function(){

    JTUI.accordions.push( new JTUI.Accordion( this ) );
  });

  $(".jt-carousel").each(function(){

    JTUI.carousels.push( new JTUI.Carousel( this ) );
  });

  $(".jt-slider-carousel").each(function(){

    JTUI.slider_carousels.push( new JTUI.SliderCarousel( this ) );
  });

  $(".jt-height-matcher").each(function(){

    JTUI.height_matchers.push( new JTUI.HeightMatcher( this ) );
  });

  JTUI.getUserAgent();

  $(window).on( "scroll", JTUI.handleScroll );

  $(window).on( "resize", JTUI.handleResize );

  console.log( "JTUI Ready!" );
  
  console.log(JTUI);

  window.requestAnimationFrame( JTUI.step );
});
