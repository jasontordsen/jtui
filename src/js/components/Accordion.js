
/* === ACCORDION =========================== */
/* ========================================= */

JTUI.Accordion = function( _el ){

  this.el = _el;

  this.$el = $(this.el);

  this.groups = this.$el.find(">.group");

  this.$el.find("button").click( this.handleClick.bind(this) );

  this.collapse = this.$el.data("collapse");

  if( this.collapse === false ) this.toGroup( 0 );
}

JTUI.Accordion.prototype.handleClick = function(e){
  
  var _index = $(e.currentTarget).parent(".group").index();

  this.toGroup( _index );
}

JTUI.Accordion.prototype.toGroup = function( _index ){

    if( this.collapse !== false || this.active !== _index  )
      this.deactActGroup();

    if( this.active !== _index  ){

      this.active = _index;

      this.actActGroup();
    }
}

JTUI.Accordion.prototype.deactActGroup = function(){

  if( this.active !== undefined && this.active !== null ){

    var _group = this.groups.eq( this.active ),

    _panel = _group.find(">.panel").eq(0);

    if ( _group.hasClass("active") )
      _group.removeClass("active");

    if( _panel ) _panel.css( "max-height", "0px" );

    this.dispatchResize();
  }
}

JTUI.Accordion.prototype.actActGroup = function(){

  if( this.active !== undefined && this.active !== null ){

    var _group = this.groups.eq( this.active ),

    _panel = _group.find(">.panel").eq(0);

    if ( !_group.hasClass("active") )
      _group.addClass("active");

    if( _panel ) _panel.css( "max-height", _panel.children().eq(0).outerHeight() + "px" );

    this.dispatchResize();
  }
}

JTUI.Accordion.prototype.dispatchResize = function(){

  if( this.timeout ) clearTimeout( this.timeout );

  this.timeout = setTimeout( JTUI.onDidResize, 300 );
}