
/* === CAROUSEL =========================== */
/* ========================================= */

JTUI.Carousel = function( _el ){

  this.activeSlide = null;

  this.el = _el;

  this.$el = $(this.el);

  this.$inner_el = this.$el.find(">.jt-carousel-inner").eq(0);

  this.slides = this.$inner_el.find(">.jt-carousel-slide");

  this.dot_nav_el = this.$el.find("ul.jt-dot-nav")[0];

  this.arrow_nav_el = this.$el.find("ul.jt-arrow-nav")[0];

  this.dot_nav = new JTUI.DotNav( this.slides.length, this.dot_nav_el, this.toSlide.bind(this) );

  this.arrow_nav = new JTUI.ArrowNav( this.arrow_nav_el, this.onArrowClick.bind(this) );

  this.swiper = new JTUI.Swiper( this.el, this.onSwipe.bind(this) );

  this.$inner_el.css({
		"width": this.slides.length*100 + "%",
		"max-height" : this.$inner_el.outerHeight()
	});

  this.slides.css("width", 100/this.slides.length + "%");

  this.toSlide(0);
}

JTUI.Carousel.prototype.onResize = function(){
	
	if( this.activeSlide != null ){

		var slide = this.slides.eq( this.activeSlide );

		this.$inner_el.css({
			"max-height" : slide.outerHeight()
		});
  	}
}

JTUI.Carousel.prototype.onArrowClick = function( _direction ){

	this.onSwipe( _direction == "left" ? "right" : "left" );
}

JTUI.Carousel.prototype.onSwipe = function( _direction ){

	if(!_direction) return null;

	switch( _direction ){
		case "right":
			this.previousSlide();
		break;

		case "left":
			this.nextSlide();
		break;
	}
}

JTUI.Carousel.prototype.toSlide = function( _index ){

	if(this.activeSlide !== _index){

		this.deactActiveSlide();

		this.activeSlide = _index;

		this.actActiveSlide();
	}
}

JTUI.Carousel.prototype.nextSlide = function( _index ){

	this.deactActiveSlide();

	this.activeSlide++;

	if( this.activeSlide > this.slides.length-1 ) 
		this.activeSlide = 0;

  	this.actActiveSlide();
}

JTUI.Carousel.prototype.previousSlide = function( _index ){

  	this.deactActiveSlide();

	this.activeSlide--;

	if( this.activeSlide < 0 ) 
		this.activeSlide = this.slides.length-1;

  	this.actActiveSlide();
}

JTUI.Carousel.prototype.deactActiveSlide = function( ){

	if( this.activeSlide != null )
		this.slides.eq(this.activeSlide).removeClass("active");
}

JTUI.Carousel.prototype.actActiveSlide = function( ){

	if( this.activeSlide != null ){

		var slide = this.slides.eq( this.activeSlide );

	  	if( !slide.hasClass("active") )
  			slide.addClass("active")

  		this.dot_nav.setActive( this.activeSlide );

		var x = "translateX(" + (-100/this.slides.length*(this.activeSlide)) + "%)";
		  
  		this.$inner_el.css( { 
			"max-height" : slide.outerHeight(),
  			"transform":x, 
  			"-webkit-transform":x,
  			"-ms-transform":x,
  			"-moz-transform":x 
  		} );
  }
}


