
/* === DROPDOWN ============================ */
/* ========================================= */

JTUI.Dropdown = function( _el ){

  this.el = _el;

  this.$el = $(this.el);

  this.$content_el = this.$el.find(">.dropdown-content").eq(0);

  this.$content_inner_el = this.$content_el.children().eq(0);

  this.$button_el = this.$el.find(".trigger");

  this.$button_el.click( this.handleClick.bind(this) );
}

JTUI.Dropdown.prototype.handleClick = function(e){

  if ( this.$el.hasClass("active") ) {

    this.$el.removeClass("active");

    this.$content_el.css("max-height", "0");

  } else {

    this.$el.addClass("active");

    var _ci_height = this.$content_inner_el.outerHeight();

    this.$content_el.css("max-height", _ci_height + "px");
  }
}

JTUI.Dropdown.prototype.onResize = function(){

  if ( this.$el.hasClass("active") ) {

    this.$content_el.css("max-height", this.$content_inner_el.outerHeight() + "px");
  }
}