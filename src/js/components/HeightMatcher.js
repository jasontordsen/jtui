/* === HEIGHT WATCHER ============================== */
/* ========================================= */

JTUI.HeightMatcher = function( _el ){

    this.el = _el;

    this.$el = $(this.el);

    this.children = this.$el.find(">.jt-match-height");

    this.onResize();
  }
  
  JTUI.HeightMatcher.prototype.onResize = function(e){
    var _row_heights = [], _els = [];

	//Set natural height, then set to 150 to get desired row number.
    this.children.each(function(i, v) {
      var _el = $(v);

      _el.height("auto");

      _els.push({ _el: _el, natural_height: _el.outerHeight() });

      _el.height(150);
    });

    // Get row numbers
    $.each(_els, function(i, v) {
      v.row = Math.abs(Math.ceil(v._el.offset().top / 10) * 10);

      if (!_row_heights[v.row] || _row_heights[v.row] < v.natural_height)
        _row_heights[v.row] = v.natural_height;
    });

    // Set the height to row height
    $.each(_els, function(i, v) {
      v._el.height(_row_heights[v.row]);
    });
  }
