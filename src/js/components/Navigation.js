
/* === DOT NAV =========================== */
/* ========================================= */

JTUI.DotNav = function( _length, _el, _onClick ){

	this.activeIndex = null;

	this.$el = $(_el);

	this.handleClick = _onClick;

	this.update( _length );
}

JTUI.DotNav.prototype.update = function( _length ){
	if( this.dots ){
		$.each( this.dots, function(i, dot){
			$(dot).off();
		});
	}

	this.$el.empty();

	this.dots = [];

	for( var i = 0; i<_length; i++ ){
		var _dot = $("<li>");

		_dot.click(function(e){
			this.handleClick( $(e.target).index() );
		}.bind( this ));

		this.$el.append(_dot);

		this.dots.push( _dot );
	}

	this.setActive( this.activeIndex || 0 );
}

JTUI.DotNav.prototype.setActive = function( _index ){
	if( this.activeIndex !== null )
		this.dots[ this.activeIndex ].removeClass("active");

	this.activeIndex = _index;

	if( !this.dots[ this.activeIndex ].hasClass("active") )
		this.dots[ this.activeIndex ].addClass("active");
}

/* === ARROW NAV =========================== */
/* ========================================= */

JTUI.ArrowNav = function( _el, _onClick ){

	this.$el = $(_el);

	this.$el.find(">li").click( function(e){

		_onClick( $(e.currentTarget).index() == 0 ? "left" : "right" );
	});
}