
/* === SLIDER CAROUSEL =========================== */
/* ========================================= */

JTUI.SliderCarousel = function( _el ){

    this.activeSlide = null;

    this.columns = 3;

    this.slideBy = this.columns-1;

    this.timeout_delay = 100;

    this.el = _el;
  
    this.$el = $(this.el);
  
    this.$slides_el = this.$el.find(">.jt-carousel-content>.jt-carousel-slides-wrapper>.jt-carousel-slides").eq(0);
  
    this.slides = this.$slides_el.find(">.jt-carousel-slide");

    this.num_slides = this.slides.length;

    this.dot_nav_el = this.$el.find("ul.jt-dot-nav")[0];
  
    this.arrow_nav_el = this.$el.find("ul.jt-arrow-nav")[0];
  
    this.arrow_nav = new JTUI.ArrowNav( this.arrow_nav_el, this.onArrowClick.bind(this) );
  
    this.swiper = new JTUI.Swiper( this.el, this.onSwipe.bind(this) );
  
    this.build();
  }

  JTUI.SliderCarousel.prototype.build = function(){

    if( this.num_slides < this.columns ) this.columns = this.num_slides; 

    $.each( this.slides, function(){
        $( this ).attr( "data-slide-index", $(this).index() );
    });

    if( this.dot_nav ){
        
        this.dot_nav.update( this.num_slides / this.slideBy );
    } else {
        
        this.dot_nav = new JTUI.DotNav( 
            this.num_slides / this.slideBy, 
            this.dot_nav_el, 
            this.toSlide.bind(this)
        );
    }

    this.end_clones = this.slides.slice( 0, this.columns * 2 ).clone().addClass( "clone" );

    this.start_clones = this.slides.slice( -this.columns*2 ).clone().addClass("clone");

    this.$slides_el.prepend( this.start_clones ).append( this.end_clones );

    this.slides = this.$slides_el.find( ">.jt-carousel-slide" );

    this.$slides_el.css({
        "width": ( this.slides.length / this.columns ) * 100 + "%"
    });

    this.slides.css("width", 100/this.slides.length + "%");

    this.toFirstRealSlide();
  }
  
  JTUI.SliderCarousel.prototype.toFirstRealSlide = function(){
    
    if( !this.$slides_el.hasClass( "kill-transition") )
        this.$slides_el.addClass( "kill-transition");

    this.activeSlide = this.start_clones.length + 1 - this.columns;

    this.actActiveSlide();

    this.next_prev_timeout = setTimeout( this.next.bind(this), this.timeout_delay );
  }

  JTUI.SliderCarousel.prototype.onResize = function(){
      
  }
  
  JTUI.SliderCarousel.prototype.onArrowClick = function( _direction ){
  
      this.onSwipe( _direction == "left" ? "right" : "left" );
  }
  
  JTUI.SliderCarousel.prototype.onSwipe = function( _direction ){
  
      if(!_direction) return null;
    
      if(this.next_prev_timeout ) clearTimeout( this.next_prev_timeout );

      switch( _direction ){
          case "right":
            this.next_prev_timeout = setTimeout( this.previous.bind(this), this.timeout_delay );
          break;
  
          case "left":
            this.next_prev_timeout = setTimeout( this.next.bind(this), this.timeout_delay );
          break;
      }
  }
  
  JTUI.SliderCarousel.prototype.toSlide = function( _index ){
  
    if(this.activeSlide !== _index){
  
        this.activeSlide = _index;
  
        this.actActiveSlide();
    }
  }
  
  JTUI.SliderCarousel.prototype.next = function(){
    
    var first_real_index = this.start_clones.length,
    
    last_real_index = first_real_index + this.num_slides - 1;
    
    if( this.activeSlide > last_real_index ){

        var real_slide_index =  first_real_index + (this.activeSlide - (last_real_index + 1));

        if( !this.$slides_el.hasClass( "kill-transition") )
            this.$slides_el.addClass( "kill-transition");

        this.activeSlide = real_slide_index;

        this.actActiveSlide();

        if(this.next_prev_timeout ) clearTimeout( this.next_prev_timeout );

        this.next_prev_timeout = setTimeout( this.next.bind(this), this.timeout_delay );
    } else {

        if( this.$slides_el.hasClass( "kill-transition") )
            this.$slides_el.removeClass( "kill-transition");

        this.activeSlide += this.slideBy;

        this.actActiveSlide();
    }
  }
  
  JTUI.SliderCarousel.prototype.previous = function(){

    var first_real_index = this.start_clones.length;

    if( this.activeSlide <= first_real_index - this.columns ){

        var real_slide_index =  this.start_clones.length + this.activeSlide;

        if( !this.$slides_el.hasClass( "kill-transition") )
            this.$slides_el.addClass( "kill-transition");

        this.activeSlide = real_slide_index;

        this.actActiveSlide();

        if(this.next_prev_timeout ) clearTimeout( this.next_prev_timeout );

        this.next_prev_timeout = setTimeout( this.previous.bind(this), this.timeout_delay );
    } else {

        if( this.$slides_el.hasClass( "kill-transition") )
            this.$slides_el.removeClass( "kill-transition");

        this.activeSlide -= this.slideBy;

        this.actActiveSlide();
    }
  }
  
  JTUI.SliderCarousel.prototype.actActiveSlide = function( ){
  
    if( this.activeSlide != null ){

        var slide_x = "translateX(" + this.activeSlide * ( -100 / this.slides.length ) + "%)";
            
        this.$slides_el.css( { 
            "transform":slide_x, 
            "-webkit-transform":slide_x,
            "-ms-transform":slide_x,
            "-moz-transform":slide_x 
        } );
    }
  }