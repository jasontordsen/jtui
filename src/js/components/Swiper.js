

/* === SWIPER ============================== */
/* ========================================= */

JTUI.Swiper = function( _el, _handleSwipe ){

  this.start = null;

  this.end = null;

  this.onSwipe = _handleSwipe;

  $(_el).on( 'touchstart', this.onTouchStart.bind(this) );
  $(_el).on( 'touchmove', this.onTouchMove.bind(this) );
  $(_el).on( 'touchend', this.onTouchEnd.bind(this) );
}

JTUI.Swiper.prototype.onTouchStart = function(e){

    if( e.touches && e.touches[0] ){

        this.start = e.touches[0];

        this.start.time = e.timeStamp;
    }
}

JTUI.Swiper.prototype.onTouchMove = function( e ){

    if( e.touches && e.touches[0] ){

        this.end = e.touches[0];

        this.end.time = e.timeStamp;
    }
}

JTUI.Swiper.prototype.onTouchEnd = function( e ){

    if( this.start && this.end ){

        var direction = null, duration = this.end.time - this.start.time;

        if( duration > 100 && duration < 400 ){

            dist = { 
                x : this.end.clientX - this.start.clientX, 
                y : this.end.clientY - this.start.clientY 
            }

            if( Math.abs( dist.x ) > 100 && Math.abs( dist.y ) < 50 ){

                direction = dist.x < 0 ? "left" : "right";

            } else if( Math.abs( dist.y ) > 50 ){

                direction = dist.y > 0 ? "down" : "up";
            }

            if( direction ) this.onSwipe( direction );

            this.touch = null;
        }
    }
}