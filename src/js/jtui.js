
/* === INIT =============================== */
/* ========================================= */

$(function() {

  $(".jt-dropdown").each(function(){

    JTUI.dropdowns.push( new JTUI.Dropdown( this ) );
  });

  $(".jt-accordion").each(function(){

    JTUI.accordions.push( new JTUI.Accordion( this ) );
  });

  $(".jt-carousel").each(function(){

    JTUI.carousels.push( new JTUI.Carousel( this ) );
  });

  $(".jt-slider-carousel").each(function(){

    JTUI.slider_carousels.push( new JTUI.SliderCarousel( this ) );
  });

  $(".jt-height-matcher").each(function(){

    JTUI.height_matchers.push( new JTUI.HeightMatcher( this ) );
  });

  JTUI.getUserAgent();

  $(window).on( "scroll", JTUI.handleScroll );

  $(window).on( "resize", JTUI.handleResize );

  console.log( "JTUI Ready!" );
  
  console.log(JTUI);

  window.requestAnimationFrame( JTUI.step );
});
