
/* === STATE =============================== */
/* ========================================= */

var JTUI = {

	didresize : true,

	didscroll : true,

	resizeto : null,

	mousedidmove : false,

	browser : {},

	currentframe : 0,

	laststeptime : 0,

	dropdowns : [],

	accordions : [],

	carousels : [],

	slider_carousels : [],

	height_matchers : []
}

/*======== EVENT HANDLERS ==========
===================================*/

JTUI.handleScroll = function( _e ) {

	JTUI.didscroll = true;
}

JTUI.onDidScroll = function() {

}

JTUI.handleResize = function( _e ) {

	clearTimeout( JTUI.resizeto );

	JTUI.resizeto = setTimeout( function() {

		JTUI.didresize = true;
	}, 1000);
}

JTUI.onDidResize = function() {
	console.log("onDidResize");

	$.each(JTUI.height_matchers, function(){
		this.onResize();
	});

	$.each(JTUI.carousels, function(){
		this.onResize();
	});

	$.each(JTUI.dropdowns, function(){
		this.onResize();
	});

	JTUI.onDidScroll();
}

/*======== ANIMATION LOOP ==========
  ===================================*/

JTUI.step = function(time) {
	if ( Math.round(JTUI.currentframe) % 20 === 0 && JTUI.didresize ) {

	    JTUI.onDidResize();

	    JTUI.didscroll = true;

	    JTUI.didresize = false;
	}

	if ( Math.round(JTUI.currentframe) % 20 === 0 && JTUI.didscroll ) { 

	    JTUI.onDidScroll();

	    JTUI.didscroll = false;
	}

	JTUI.laststeptime = time;

	JTUI.currentframe++;

	window.requestAnimationFrame( JTUI.step );
}