
/* === UTILS ============================ */
/* ====================================== */

JTUI.getUserAgent = function() {

    JTUI.useragent = navigator.userAgent;

    //MOBILE
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        JTUI.useragent
      )
    )
      mobile = true;

    //GET BROWSER NAME
    if ( /Mozilla/i.test(JTUI.useragent) ) JTUI.browser.name = "Firefox";
    if ( /Safari/i.test(JTUI.useragent) ) JTUI.browser.name = "Safari";
    if ( /Chrome/i.test(JTUI.useragent) ) JTUI.browser.name = "Chrome";
    if ( /Android/i.test(JTUI.useragent) ) JTUI.browser.name = "Android";
    if ( /MSIE|Trident/i.test(JTUI.useragent) ) JTUI.browser.name = "IE";

    //GET BROWSER VERSION
    var start_i = 0,
    dot_i = 0,
    v_search_str = null;

    switch (JTUI.browser.name) {
      case "Firefox":
        JTUI.browser.prefix = { js: "Moz", css: "-moz-" };
        v_search_str = "Firefox/";
        break;
      case "Safari":
        JTUI.browser.prefix = { js: "webkit", css: "-webkit-" };
        v_search_str = "Version/";
        break;
      case "Chrome":
        JTUI.browser.prefix = { js: "webkit", css: "-webkit-" };
        v_search_str = "Chrome/";
        break;
      case "Android":
        JTUI.browser.prefix = { js: "webkit", css: "-webkit-" };
        v_search_str = "Android ";
        break;
      case "IE":
        JTUI.browser.prefix = { js: "ms", css: "-ms-" };
        if (/MSIE/i.test(JTUI.useragent)) v_search_str = "MSIE ";
        else JTUI.browser.version = "11.0";
        break;
    }

    if ( v_search_str ) {

      start_i = JTUI.useragent.indexOf(v_search_str);

      dot_i = JTUI.useragent.indexOf(".", start_i);

      JTUI.browser.version = JTUI.useragent.substring(
        start_i + v_search_str.length,
        dot_i + 2
      );
    }
  }